/**
 * Punto de inicio del backend
 */
class Main{

	/**
	 * Inicializació de la clase
	 */
	constructor(){
		this.loadDependencies();
		this.serveStaticFiles();
		this.loadLibs();
		this.initSocket();
	}

	/**
	 * Carga las dependencias
	 */
	loadDependencies(){
		this.express = require("express");
		this.app = this.express();
		this.http = require("http").Server(this.app);
		this.io = require("socket.io")(this.http);
	}

	loadLibs(){
		this.Socket = require("./libs/socket.js");
		this.socket = new this.Socket();
	}

	/**
	 * Configura Express para que sirva el frontend en la raíz
	 */
	serveStaticFiles(){
		//this.app.use('/socket.io/', this.express.static('./node_modules/socket.io-client/dist/'));
		this.app.use('/', this.express.static('../frontend/dist/'));
		this.http.listen(80, this.onServeStaticFiles.bind(this))
	}

	/**
	 * Recibe el resultadod de iniciar la escucha del puerto del web server
	 */
	onServeStaticFiles(){
		console.log("jOS web server is running at port 80");
	}

	/**
	 * Inicializa socket.io
	 */
	async initSocket(){
		await this.socket.asyncInit();
		await this.socket.setIO(this.io);
		this.io.on("connection", this.socket.onConnect.bind(this.socket));
	}
}


new Main();