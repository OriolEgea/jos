class Socket{

	constructor(){
		this.users = [];
		const Persist = require("./persist.js");
		this.persist = new Persist();
	}

	async asyncInit(){
		try{
			await this.persist.init();
		}
		catch(exception){
			console.error(exception);
		}
	}

	async setIO(io){
		this.io = io;
	}

	onConnect(socket){
		this.users.push(socket);
		console.log(`${socket.id} connected`);

		//Comandos
		socket.on("disconnect", this.onDisconnect.bind(this, socket));
		socket.on("install", this.onInstall.bind(this, socket));
		socket.on("login", this.onLogin.bind(this, socket));
		socket.on("listApps", this.onListApps.bind(this, socket));

		//Si el sistema no está instalado le pedimos al cliente que empiece la instalación
		if (this.persist.data.system.users.length === 0){
			socket.emit('install', '');
		}
		//Si el sistema está instalado, le reclamamos login
		else if (this.persist.data.system.users.length !== 0){
			socket.emit('login', '');
		}
		
	}

	onDisconnect(socket){
		console.log(`${socket.id} disconnected`);
		for (let i in this.users){
			if (this.users[i].id === socket.id){
				console.log(`${socket.id} has been deleted from the users array`);
				this.users.splice(i, 1);
				break;
			}
		}
	}

	async onInstall(socket, data){
		await this.persist.install(data);
		socket.emit('restart', '');
	}

	onLogin(socket, data){

		console.log(socket.id+" requested login as "+data.username);
		//Si ya estamos logados, no pasa nada
		if (socket.username !== undefined && socket.username !== null && socket.username !== ""){
			console.log(socket.id+" is already logged in the system");
			return;
		}


		console.log(socket.id+" is not logged");
		//Comprobamos si el password de usuario es válido
		let validLogin = this.persist.isValidLogin(data.username, data.password);

		if (validLogin === true){
			console.log(socket.id+" has logged as "+data.username);
			socket.username = data.username;
			socket.emit('logged', data.username);
		}else{
			console.log(socket.id+" has provided invalid login data");
			socket.emit('notLogged', '');
		}
	}

	onListApps(socket, data){
		console.log("Listing apps for "+socket.username);
		socket.emit("listApps", this.persist.data.system.apps);
	}
}

module.exports = Socket;