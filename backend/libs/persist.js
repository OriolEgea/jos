class Persist{

	constructor(){
		this.VERSION = 1003;
		this.DB_NAME = "jOSDB";
		this.storage = require("node-persist");
		this.hash = require("password-hash");
	}

	async init(){
		await this.storage.init();

		let data = await this.storage.getItem(this.DB_NAME);
		await this.loadData(data);
		console.log("Database initialized");
	}

	async loadData(data){
		try{
			//Si data es null, asignamos un valor vacío
			if (data === null || data === undefined){
				data = "";
			}
			//Parseamos a objeto data
			this.data = JSON.parse(data);
		}catch(exception){
			//Si se ha producido algún error, montamos la estructura base de la base de datos
			this.data = {
				system: {
					version: this.VERSION,
					name: null,
					build: null,
					apps: [],
					users:[]
				}
			};

			//Aplicaciones pre-instaladas
			this.data.system.apps.push({
				"name": "About jOS",
				"icon": "info",
				"packageName": "sysinfo",
				"packageWebsite": "www.j-os.org",
				"author": "Oriol Egea",
				"authorWebsite": "www.oriol.im"
			});

			this.data.system.apps.push({
				"name": "System configuration",
				"icon": "settings",
				"packageName": "sysconfig",
				"packageWebsite": "www.j-os.org",
				"author": "Oriol Egea",
				"authorWebsite": "www.oriol.im"
			});

			//Guardamos esta estructura base
			await this.save();
		}
	}

	async save(){
		const data = JSON.stringify(this.data);
		await this.storage.setItem(this.DB_NAME, data);
	}

	async install(data){
		this.data.system.users.push({
			username: data.username,
			password: this.hash.generate(data.password),
			admin: true
		})
		this.data.system.build = this.VERSION;

		await this.save();

		return true;
	}

	isValidLogin(username, password){
		//Iteramos usuarios...
		for (let i in this.data.system.users){
			let user = this.data.system.users[i];
			if (user.username === username){
				if (this.hash.verify(password, user.password) === true)
					return true;
				break;
			}
		}

		return false;
	}

}

module.exports = Persist;