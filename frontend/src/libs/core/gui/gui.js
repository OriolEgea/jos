//Dependencias
import $ from "jquery";

//Librerías
import Window from "./window.js";

/**
 * Clase para gestionar y almacearel estado de la interfaz gráfica
 */
class Gui{
	/**
	 * CTOR
	 * @return {Object}
	 */
	constructor(core){
		//Dependencias
		this.core = core;

		//Placeholder en el que dibujar las ventanas
		this.$placeholder = $("body");
		this.explorerLoaded = false;

		//En un array almacenamos todas las ventanas abiertas
		this.windows = [];

		//Definimos los eventos de la interfaz
		this.setEvents();

		//Consideramos inicializada la gui, borramos la console
		this.$placeholder.find("#console").remove();
		this.$placeholder.attr("class", "gui-active");
	}

	setEvents(){
		$(window).on("mouseup", this.onMouseUp.bind(this));
	}

	refreshPlaceholder(){
		let placeholder = $(".explorer-windows-box");
		if(placeholder.length > 0) {
			this.$placeholder = placeholder;
			this.explorerLoaded = true;
		}
	}

	onMouseUp(){
		//Al soltar el ratón, cualquier ventana que estuviésemos moviendo, para de moverse
		for (let i in this.windows){
			const thisWindow = this.windows[i];
			if (thisWindow.dragging === true){
				thisWindow.onMouseUp();
			}
		}
	}

	addWindow(title, processId, options){
		if(this.explorerLoaded == false)
			this.refreshPlaceholder();

		let width = 800;
		let height = 600;

		//Si han definido correctamente el ancho y el alto
		if (typeof options === "object" && options.hasOwnProperty("width") && Number.isInteger(options.width) && options.width > 0){
			width = options.width;
		}	

		if (typeof options === "object" && options.hasOwnProperty("height") && Number.isInteger(options.height) && options.height > 0){
			height = options.height;	
		}
		
		//Iniciamos la window
		let newWindow = new Window(title, width, height, processId, this.$placeholder, this.core);
		this.windows.push(newWindow);
		return newWindow;
	}

	closeWindow(windowId){
		let processId = null;

		//Iteramos la lista de ventanas para quitarla
		for (let i in this.windows){
			let windowData = this.windows[i];
			if (windowData.id === windowId){
				//Antes de borrarlo nos quedamos con su identificador de proceso
				processId = windowData.processId;
				this.windows.splice(i,1);
				break;
			}
		}

		//Si el processId no es null, notificamos al processManager que se ha cerrado el diálogo
		if (processId !== null){
			this.core.processManager.closeWindow(windowId, processId);
		}
	}
}

export default Gui;