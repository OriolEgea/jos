//Dependencias
import $ from 'jquery';
//Librerías
import Utilities from './../utilities';

class Window extends Utilities{
	constructor(title, width, height, processId, $placeholder, core){
		super();
		const windowHtml = require("./window.html");
		//Dependencias
		this.core = core;

		//Propiedades
		this.id = this.uuid();
		this.processId = processId;
		this.$placeholder = $placeholder;

		//Cargamos el html básico de una ventana
		this.$window = $(windowHtml);

		//Estado de la ventana
		this.title = title;
		this.width = width;
		this.height = height;
		this.minimized = false;
		this.maximized = false;
		this.resizing = false; //True si estamos redimensionando la ventana
		this.dragging = false; //True si estamos moviendo la ventana

		this.dragPosition = {
			x: 0,
			y: 0
		}; //Define desde qué punto ha empezado el drag, para mantener la posición de la ventana

		//Llamamos a los métodos correspondientes
		this.setProperties(title);
		this.print();
		this.setEvents();
	}


	/**
	 * Define las propiedades de una ventana, tales cómo título, etcétera
	 */
	setProperties(title){
		this.$window.attr("data-uuid", this.id);
		this.$window.find("div[data-type='title']").html(title);
	}

	/**
	 * Define los eventos para poder mover y cerrar una ventana
	 */
	setEvents(){
		//Al pulsar la barra de título
		const $toolbar = this._getToolbarEl();
		$toolbar.off("mousedown");
		$toolbar.off("mouseup");
		$toolbar.on("mousedown", this.onMouseDown.bind(this));

		//Al pulsar el botón de cerrar, cerramos la ventana
		const $close = this._getCloseEl();
		$close.off("click");
		$close.on("click", this.onWindowClose.bind(this));

		//Al pulsar el botón de minimizar, ocultamos
		//const $minimize = this._getMinimizeEl();
		//$minimize.off("click");
		//$minimize.on("click", this.onWindowMinimize.bind(this));

		//Al pulsar en cualquier parte de la ventana, la seleccionamos
		const $window = this._getWindowEl();
		$window.off("click");
		$window.on("click", this.setFocus.bind(this, $window));

		const $minimize = this._getMinimizeEl();
		$minimize.off("click");
		$minimize.on("click", this.onWindowMinimize.bind(this));

		const $maximize = this._getMaximizeEl();
		$maximize.off("click");		
		$maximize.on("click", this.onWindowMaximize.bind(this));

		this.setResizingEvents();

	}

	setResizingEvents(){
		//Al pulsar en el left-window-resize
		const $leftWindowResize = this._getLeftWindowResize();
		$leftWindowResize.off("mousedown");
		$leftWindowResize.off("mouseup");
		$leftWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "left"));
		//Al pulsar en el right-window-resize
		const $rightWindowResize = this._getRightWindowResize();
		$rightWindowResize.off("mousedown");
		$rightWindowResize.off("mouseup");
		$rightWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "right"));
		//Al pulsar en el top-window-resize
		const $topWindowResize = this._getTopWindowResize();
		$topWindowResize.off("mousedown");
		$topWindowResize.off("mouseup");
		$topWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "top"));
		//Al pulsar en el bottom-window-resize
		const $bottomWindowResize = this._getBottomWindowResize();
		$bottomWindowResize.off("mousedown");
		$bottomWindowResize.off("mouseup");
		$bottomWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "bottom"));

		const $cornerTopLeftWindowResize = this._getCornerTopLeftWindowResize();
		$cornerTopLeftWindowResize.off("mousedown");
		$cornerTopLeftWindowResize.off("mouseup");
		$cornerTopLeftWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "cornerTopLeft"));

		const $cornerTopRightWindowResize = this._getCornerTopRightWindowResize();
		$cornerTopRightWindowResize.off("mousedown");
		$cornerTopRightWindowResize.off("mouseup");
		$cornerTopRightWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "cornerTopRight"));

		const $cornerBottomLeftWindowResize = this._getCornerBottomLeftWindowResize();
		$cornerBottomLeftWindowResize.off("mousedown");
		$cornerBottomLeftWindowResize.off("mouseup");
		$cornerBottomLeftWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "cornerBottomLeft"));

		const $cornerBottomRightWindowResize = this._getCornerBottomRightWindowResize();
		$cornerBottomRightWindowResize.off("mousedown");
		$cornerBottomRightWindowResize.off("mouseup");
		$cornerBottomRightWindowResize.on("mousedown", this.onResizeMouseDown.bind(this, "cornerBottomRight"));

	}

	print(){
		//Cualquier otra window deja de estar activa
		this.setFocus(this.$window);

		this.$window.css("width", this.width+"px");
		this.$window.css("height", this.height+"px");
		this.$placeholder.append(this.$window);
	}

	setFocus($window){
		if ($window === undefined)
			$window = this.$window;

		if (this.minimized !== true){
			$(".window").removeClass("active");
			$window.addClass("active");

			$window.show();
		}

	}

	_getWindowEl(){
		return $("body").find(`[data-uuid='${this.id}']`);
	}
	_getToolbarEl(){
		return $("body").find(`[data-uuid='${this.id}'] .toolbar`);
	}

	_getMinimizeEl(){
		return $("body").find(`[data-uuid='${this.id}'] .toolbar [data-type='minimize']`);
	}

	_getMaximizeEl(){
		return $("body").find(`[data-uuid='${this.id}'] .toolbar [data-type='maximize']`);
	}

	_getCloseEl(){
		return $("body").find(`[data-uuid='${this.id}'] .toolbar [data-type='close']`);
	}

	_getWindowBody(){
		return $("body").find(`[data-uuid='${this.id}'] .window-content`);
	}

	_getLeftWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .left_handler_horizontal`);
	}

	_getRightWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .right_handler_horizontal`);
	}

	_getTopWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .top_handler_vertical`);
	}

	_getBottomWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .bottom_handler_vertical`);
	}

	_getCornerTopLeftWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .corner_top_left`);
	}

	_getCornerTopRightWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .corner_top_right`);
	}

	_getCornerBottomLeftWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .corner_bottom_left`);
	}

	_getCornerBottomRightWindowResize(){
		return $("body").find(`[data-uuid='${this.id}'] .corner_bottom_right`);
	}

	onMouseDown(event){
		const $windowEl = this._getWindowEl();

		//¿Dónde está la ventana?
		const left = parseInt($windowEl.css("left")); //Esto corresponde a clientX
		const top = parseInt($windowEl.css("top")); //Esto corresponde a clientY

		//Calculamos la diferencia entre left y clientX y top y clientY
		this.dragPosition.x = event.clientX-left;
		this.dragPosition.y = event.clientY-top;

		//console.log("Empieza a moverse ventana");
		this.dragging = true;
		//Al pulsar en la barra, creamos un evento para cuando se mueva el ratón
		
		$(window).on("mousemove", this.onMouseMove.bind(this));

		this.setFocus($windowEl);
	}
	onMouseMove(event){
		//Obtenemos la ventana
		const $windowEl = this._getWindowEl();

		//Movemos
		$windowEl.css("left", `${(event.clientX-this.dragPosition.x)}px`);
		$windowEl.css("top", `${(event.clientY-this.dragPosition.y)}px`);
	}
	onMouseUp(){
		this.dragging = false;
		this.resizing = false;
		$(window).off("mousemove");
	}

	onResizeMouseDown(mode){
		const $windowEl = this._getWindowEl();

		const width = 0;
		const height = 0;

		//Guardamos la posicion actual del cursor
		this.dragPosition.x = event.clientX-width;
		this.dragPosition.y = event.clientY-height;

		this.resizing = true;

		$(window).on("mousemove", this.onResizeMouseMove.bind(this, mode));
		$(window).on("mouseup", this.onMouseUp.bind(this));
	}

	onResizeMouseMove(mode){
		//Obtenemos la ventana
		const $windowEl = this._getWindowEl();

		let windowWidth = parseInt($windowEl.css("width"));
		let windowLeft = parseInt($windowEl.css("left"));
		let windowHeight = parseInt($windowEl.css("height"));	
		let windowTop = parseInt($windowEl.css("top"));
				
		console.log(mode);

		//Movemos
		switch(mode){
			case "left":
				//Modificar width y left
				$windowEl.css("width", `${(windowWidth + (this.dragPosition.x-event.clientX))}px`);
				$windowEl.css("left", `${(windowLeft + (event.clientX-this.dragPosition.x))}px`);
			break;
			case "right":				//let windowWidth = parseInt($windowEl.css("width"));
				$windowEl.css("width", `${(windowWidth + (event.clientX-this.dragPosition.x))}px`);
			break;
			case "top":
				//Modificar height y top
				$windowEl.css("height", `${(windowHeight + (this.dragPosition.y-event.clientY))}px`);
				$windowEl.css("top", `${(windowTop + (event.clientY-this.dragPosition.y))}px`);
			break;
			case "bottom":
				$windowEl.css("height", `${(windowHeight + (event.clientY-this.dragPosition.y))}px`);
			break;
			case "cornerTopLeft":
				$windowEl.css("width", `${(windowWidth + (this.dragPosition.x-event.clientX))}px`);
				$windowEl.css("left", `${(windowLeft + (event.clientX-this.dragPosition.x))}px`);			
				$windowEl.css("height", `${(windowHeight + (this.dragPosition.y-event.clientY))}px`);
				$windowEl.css("top", `${(windowTop + (event.clientY-this.dragPosition.y))}px`);
			break;
			case "cornerTopRight":
				$windowEl.css("width", `${(windowWidth + (event.clientX-this.dragPosition.x))}px`);
				$windowEl.css("height", `${(windowHeight + (this.dragPosition.y-event.clientY))}px`);
				$windowEl.css("top", `${(windowTop + (event.clientY-this.dragPosition.y))}px`);
			break;
			case "cornerBottomLeft":
				$windowEl.css("width", `${(windowWidth + (this.dragPosition.x-event.clientX))}px`);
				$windowEl.css("left", `${(windowLeft + (event.clientX-this.dragPosition.x))}px`);				
				$windowEl.css("height", `${(windowHeight + (event.clientY-this.dragPosition.y))}px`);
			break;
			case "cornerBottomRight":
				$windowEl.css("width", `${(windowWidth + (event.clientX-this.dragPosition.x))}px`);
				$windowEl.css("height", `${(windowHeight + (event.clientY-this.dragPosition.y))}px`);
			break;

		}

		//Actualizamos la posicion actual del cursor
		this.dragPosition.x = event.clientX;
		this.dragPosition.y = event.clientY;
	}

	//Funcion que cierra la ventana
	onWindowClose(){
		//Nos cargamos esta ventana
		const $window = this._getWindowEl();
		$window.remove();

		//Le decimos al gui manager que se cierra este diálogo
		this.core.gui.closeWindow(this.id);
	}

	//Funcion que minimiza la ventana
	onWindowMinimize(){
		//Para evitar errores por multipulsaciones de Oriol
		if(this.resizing) return;
		this.resizing = true;

		this.beforeMinimizeWindowStyles = this.getElementStyles(this._getWindowEl()[0]);
		if(!this.beforeMinimizeWindowStyles.top) this.beforeMinimizeWindowStyles.top = "25%";
		if(!this.beforeMinimizeWindowStyles.left) this.beforeMinimizeWindowStyles.left = "35%";
		this.oldWindowBodyStyles = this.getElementStyles(this._getWindowBody()[0]);

		this.minimized = true;
		let windowEl = this._getWindowEl();
		windowEl.animate({
		    	left: "0",
		    	top: "100%",
		    	width: "0px",
		    	height: "0px"
		  		}, 200, function() { 
		  			windowEl.hide(); 
		  			this.resizing = false;
		  		}.bind(this));
	}

	//Funcion que restaura una ventana minizada. Si ya esta restaurada la ventana la minimiza
	onWindowUnminimize(){
		if(this.minimized){
			//Para evitar errores por multipulsaciones de Oriol
			if(this.resizing) return;
			this.resizing = true;

			let windowEl = this._getWindowEl().show();
			windowEl.show();
			this.minimized = false;
			windowEl.animate(
			   	this.beforeMinimizeWindowStyles
				, 200, function() {
					this.resizing = false;
					this.setFocus();
			}.bind(this));
		}else{
			this.onWindowMinimize();
		}
	}

	//Funcion que maximiza una ventana
	onWindowMaximize(){		
		if(this.maximized){
			this.onWindowUnmaximize();
		}else{
			//Para evitar errores por multipulsaciones de Oriol
			if(this.resizing) return;
			this.resizing = true;

			this.beforeMaximizeWindowStyles = this.getElementStyles(this._getWindowEl()[0]);
			if(!this.beforeMaximizeWindowStyles.top) this.beforeMaximizeWindowStyles.top = "25%";
			if(!this.beforeMaximizeWindowStyles.left) this.beforeMaximizeWindowStyles.left = "35%";
			this.beforeMaximizeWindowBodyStyles = this.getElementStyles(this._getWindowBody()[0]);
			
			this.maximized = true;
			this._getWindowEl().animate({
		    	left: "0",
		    	top: "0",
		    	width: "100%",
		    	height: "100%"
		  		}, 200, function() {
		  			this.resizing = false;
		  		}.bind(this));
		}
	}

	//Funcion que devuelve la pantalla al tamaño normal
	onWindowUnmaximize(){
		//Para evitar errores por multipulsaciones de Oriol
		if(this.resizing) return;
		this.resizing = true;

		this.maximized = false;
			this._getWindowEl().animate(
		    	this.beforeMaximizeWindowStyles
		  		, 200, function() {
		    		this.maximized = true;
		    		this.resizing = false;
			}.bind(this));
	}

	//Devuelve un object con los parametros CSS activos del elemento
	getElementStyles(element){
		let styles = element.style;
		let endIteration = false;
		let result = {};

		for(let i=0; endIteration == false; i++){
  			let key = styles[i];
  			if(key){
  				result[key] = styles[key];
			}else{
				endIteration = true;
			}
		}

		return result;
	}
}

export default Window;