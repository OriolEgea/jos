import Utilities from "../../utilities.js";
import $ from 'jquery';

/**
 * API versión 1.0
 */
class Main extends Utilities{

	/**
	 * Inicializa el API para una aplicación en concreto
	 * @param  {Object} process Proceso que invoca la API
	 * @param  {Object} methods Métodos protegidos del core que pueden llamarse a través de la API
	 */
	constructor(process, methods){
		super();
		this.process = process;
		this.methods = methods;
	}

	//Funciones estándar

	/**
	 * Crea una nueva ventana
	 * @param  {String} title   Título de la ventana 
	 * @param  {Object} options Opciones de comportamiento de la ventana
	 * @return {Object}         Elemento jQuery de la ventana creada
	 */
	createDialog(title, options){
		let windowObj = this.methods.addWindow(title, this.process.id, options);
		this.process.dialogs.push(windowObj);
		this.methods.executeOnProcessChange();
		return windowObj._getWindowBody();
	}

	/**
	 * Obtiene un fichero de un paquete concreto a través de una llamada HTTP
	 * @param  {String}   file Carpeta y fichero final al que se quiere acceder
	 * @param  {Function} next Función a la que llamar al cargar el fichero
	 */
	getFile(file, next){
		$.get("packages/"+file, next);
	}

	/**
	 * Inserta un estilo concreto en el header de la página
	 * @param  {String} css Estilo a insertar
	 */
	loadCSS(css){
		$("head").append(`<style>${css}</style>`);
	}

	/**
	 * Modifica etiquetas de idioma por su valor correspondiente
	 * @param  {String} originalHtml HTML con las etiquetas de idioma
	 * @param  {Object} translations Objeto con los valores que deben tomar las etiquetas
	 * @return {String}              HTML parseado sin etiquetas, con los valores del idioma
	 */
	layoutLang(originalHtml, translations){
		let translatedHtml = originalHtml;

		for (let i in translations){
			translatedHtml = translatedHtml.replace(new RegExp("\\["+i+"\\]", "g"), translations[i]);
		}

		return translatedHtml;
	}

	//Funciones administrador
	
	checkAdmin(){
		if (this.process.isAdmin)
			return true;
		else
			return false;
	}
	
	/**
	 * Emite un socket al backend
	 * @param  {String}   command Comando a enviar
	 * @param  {*}   value   Parámetros del comando enviado
	 */
	emit(command, value){
		if (!this.checkAdmin())
			return;

		this.methods.emit(command, value);
	}

	/**
	 * Transfiere la recepción de un socket concreto al proceso
	 * @param  {String}   command Nombre del comando que el proceso debe interceptar
	 * @param  {Function} next    Función a la que llamar al recibir el socket
	 */
	listen(command, next){
		if (!this.checkAdmin())
			return;

		this.process.socketListeners.push({
			command: command,
			callback: next
		});
		this.methods.onSocket(command, next);
	}

	/**
	 * Obtiene el objeto de jQuery para poder manipular el DOM
	 * @param  {Function} next Función que recibirá jQuery
	 */
	getjQuery(next){
		if (!this.checkAdmin())
			return;
		next(this.methods.$);
	}

	/**
	 * Ejecuta un proceso no-privilegiado
	 * @param  {String}   package Nombre del paquete a ejecutar
	 * @param  {Function} next    Callback al que llamar cuando se realice la operación
	 */
	run(packageName, next){
		if (!this.checkAdmin())
			return;
		this.methods.run(packageName);
	}

	getProcesses(){
		if (!this.checkAdmin())
			return;

		return this.methods.getProcesses();

	}

	addProcessChangeEvent(){
		if (!this.checkAdmin())
			return;

		this.methods.addProcessChangesEvent(this.process);
	}
}

export default Main;