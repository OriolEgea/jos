class State{
	constructor(){
		this.session = null;
		this.processes = [];
		this.applications = [];
	}

	getSession(){
		return this.session;
	}

	setSession(username){
		this.session = username;;
	}
}

export default State;