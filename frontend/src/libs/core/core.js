//Dependencias
import $ from "jquery";

//Librerías
import State from "./state.js";
import ProcessManager from "./process-manager.js";
import Gui from "./gui/gui.js";

class Core{
	/**
	 * Constructor
	 */
	constructor(){
		//Constantes
		this.OS_NAME = "jOS";
		this.RELEASE = "One";
		this.PUBLIC  = false;
		this.RELEASE_TYPE = "Development"; //Development, Evaluation, etc
		this.VERSION = 1003;

		//Inicializamos el estado de la aplicación (temporal)
		this.state = new State();
		//Gestor de procesos
		this.processManager = new ProcessManager(this);
		//Inicializamos el manager de la GUI
		this.gui = new Gui(this);
		this.start();
	}

	/**
	 * Inicializa el sistema
	 */
	start(){

		//Inicializamos socket
		this.socket = io();

		//Comandos del socket
		this.socket.on("install", this.install.bind(this));
		this.socket.on("restart", this.restart.bind(this));
		this.socket.on("login", this.login.bind(this));
		this.socket.on("logged", this.logged.bind(this));
		this.socket.on("notLogged", this.notLogged.bind(this));

		//Si el sistema no es una release final...
		if (this.PUBLIC !== true){
			$("body").find("#beta-notice").html(`${this.OS_NAME} ${this.RELEASE} (${this.VERSION}) - ${this.RELEASE_TYPE} purposes`);
		}
	}

	/**
	 * Inicializa la instalación del sistema
	 */
	install(){
		if (this.processManager.processes.length > 0)
			return;
		console.log("System install started");
		this.processManager.runAsAdmin("installer");
	}

	/**
	 * Inicializa el proceso de login
	 */
	login(){
		if (this.processManager.processes.length > 0)
			return;
		console.log("System login started");
		this.processManager.runAsAdmin("login");
	}

	/**
	 * Método invocado al realizar login correcamente
	 * @param  {String} data Nombre de usuario con el que nos hemos identificado
	 */
	logged(data){
		if (this.state.getSession() === null){
			this.state.setSession(data);
			let processObj = this.processManager.getProcessObj("Login");
			if (processObj !== null){
				//Llamamos al proceso Login para que limpie la pantalla y se cierre
				processObj.module.loginSuccess();

				//Ejecutamos la interfaz de sistema
				this.processManager.runAsAdmin("desktop");
			}
		}
	}

	/**
	 * Evento sucedido al realizar un login inválido
	 */
	notLogged(){
		let processObj = this.processManager.getProcessObj("Login");

		if (processObj !== null){
			processObj.module.loginError();
		}
	}
	
	/**
	 * Reinicia por completo la aplicación
	 */
	restart(){
		document.location.href = "";
	}
}

export default Core;