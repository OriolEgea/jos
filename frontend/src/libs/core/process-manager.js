import Utilities from "./utilities.js";
import APIV1 from "./api/v1/main.js";
import $ from 'jquery';
class ProcessManager extends Utilities{

	/**
	 * Constructor del gestor de procesos
	 * @param  {Object} core Instancia del core del sistema
	 * @return {[type]}      [description]
	 */
	constructor(core){
		super();

		//Constantes
		this.PROCESS_ACTIVE = "active";
		this.PROCESS_CLOSING = "closing";

		//Propiedades
		this.nextIsAdmin = false; //En caso de estar a true, el próximo proceso iniciado tendrá privilegios admin
		
		//Referencias
		this.core = core;
		this.processes = this.core.state.processes;
		this.onProcessChangesEvents = [];

		//Función a la que se llama en caso de error
		window.unhandledError = this.run.bind(this,"api-error");
		//En caso de producirse algún error, mostramos aviso
		window.onerror = window.unhandledError;
		
	}

	//Ejecución de paquetes y creación de procesos

	/**
	 * Ejecuta un proceso sin privilegios de administrador
	 * @param  {String} file Nombre del paquete a cargar
	 */
	run(file){
		this.nextIsAdmin = false;
		this.load(file);
	}

	/**
	 * Ejecuta un proceso con privilegios de administrador
	 * @param  {String} file Nombre del paquete a cargar
	 */
	runAsAdmin(file){
		this.nextIsAdmin = true;
		this.load(file);
	}

	/**
	 * Carga un paquete para ser inicializado
	 * @param  {String} file Nombre del paquete a cargar
	 */
	load(file){
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = this.onRun.bind(this,xmlHttp);
    	xmlHttp.open("GET", "/packages/"+file+"/"+file+".js", true); // true for asynchronous 
    	xmlHttp.send(null);
	}

	/**
	 * Recibe el contenido de un paquete para ser ejecutado
	 * @param  {Object} xmlHttp Instancia de XMLHttpRequest que ha efectuado la carga del paquete
	 */
	onRun(xmlHttp){
		if (xmlHttp.readyState === 4 && xmlHttp.status === 200){
			window.addProcess = this.addProcess.bind(this);
			eval(`(function(){
				try{
					let addProcess = window.addProcess;
					window.addProcess = null;
					${xmlHttp.responseText}
				}catch(e){
					console.log(e);
					console.log("Unhandled error has ocurred");
					window.unhandledError();
				}
			}())`);
		}
		else if (xmlHttp.readyState === 4){
			window.unhandledError();
		}
	}

	/**
	 * Añade un nuevo proceso al sistema
	 * @param {String} name       Nombre del proceso
	 * @param {Number} apiVersion Versión del API utilizada
	 * @param {Object} instance   Instancia del módulo que crea el proceso
	 */
	addProcess(name, apiVersion, instance){
		//Construímos el objeto del proceso
		let processObj = this.createProcessObject(name, instance, apiVersion);

		//Dejamos de abrir procesos en modo administrador
		this.nextIsAdmin = false;

		//Instanciamos la API
		processObj.api = this.createApiInstance(apiVersion, processObj);

		//Si hemos obtenido null como instacia de API, es que se ha solicitado una versión de API no soportada
		if (processObj.api === null)
			return;

		//Añadimos el proceso al array
		this.processes.push(processObj);

		this.executeOnProcessChange();

		//Retornamos la instancia de la API
		return processObj.api;
	}

	/**
	 * Crea el objeto que define un proceso
	 * @param  {String} name       Nombre del proceso
	 * @param  {Object} instance   Instancia del módulo que crea el proceso
	 * @param  {Number} apiVersion Versión del API solicitada
	 * @return {Object}            Objeto de definición del proceso
	 */
	createProcessObject(name, instance, apiVersion){
		let processObj = {
			id: this.uuid(),
			name: name,
			status: this.PROCESS_ACTIVE,
			isAdmin: this.nextIsAdmin, 
			module: instance,
			apiVersion: apiVersion,
			api: null,
			dialogs: [],
			socketListeners: []
		};

		//Definimos funciones adicionales
		processObj.kill = this.kill.bind(this, processObj.id);

		return processObj;
	}

	/**
	 * Crea la instancia de la versión de API solicitada
	 * @param  {Number} apiVersion Versión de la API solicitada
	 * @param  {Object} processObj Objeto relativo al proceso iniciado
	 * @return {Object} Instancia de la API. Null en caso de versión no soportada.
	 */
	createApiInstance(apiVersion, processObj){
		let api = null;
		switch(apiVersion){
			case 1:
				let methods = this.getAPIV1Methods();
				api = new APIV1(processObj, methods); 
				break;

			default:
				window.unhandledError();
				break;
		}

		return api;
	}

	/**
	 * Crea un objeto con los métodos del core que encapsulará la API versión 1
	 * @return {Object} Objeto con los métodos
	 */
	getAPIV1Methods(){
		return {
			addWindow: this.core.gui.addWindow.bind(this.core.gui),
			emit: this.core.socket.emit.bind(this.core.socket),
			onSocket: this.core.socket.on.bind(this.core.socket),
			$: $,
			run: this.run.bind(this),
			processes: this.processes,
			addProcessChangesEvent: this.addProcessChangesEvent.bind(this),
			executeOnProcessChange: this.executeOnProcessChange.bind(this),
			getProcesses: ()=>{return this.processes;}
		};
	}

	//Obtención y tratamiento de procesos

	/**
	 * Obtiene el índice en el que se encuentra un proceso concreto
	 * @param  {String} processId Identificador o nombre del proceso
	 * @return {Number}           Índice en el que se encuentra el proceso
	 */
	getProcessIndex(processId){
		for (let i in this.processes){
			if (this.processes[i].id === processId || this.processes[i].name === processId){
				return i;
				break;
			}
		}
		return -1;
	}

	/**
	 * Obtiene el objeto de un proceso concreto
	 * @param  {String} processId Identificador o nombre del proceso
	 * @return {Object}           Objeto que define el proceso
	 */
	getProcessObj(processId){
		for (let i in this.processes){
			if (this.processes[i].id === processId || this.processes[i].name === processId){
				return this.processes[i];
				break;
			}
		}
		return null;
	}

	/**
	 * Mata un proceso
	 * @param  {String} processId Identificador del proceso
	 */
	kill(processId){
		let processObj = this.getProcessObj(processId);
		let processIndex = this.getProcessIndex(processId);

		//Si hemos encontrado el proceso
		if (processObj !== null){
			//Si tenemos alguna escucha de socket...
			for(let i in processObj.socketListeners){
				let socket = processObj.socketListeners;
				this.core.socket.off(socket.command, socket.callback);
			}

			//Lo marcamos como proceso que está siendo cerrado
			processObj.status = this.PROCESS_CLOSING;

			//Si el objeto tiene diálogos abiertos...
			if (processObj.dialogs.length > 0){
				//Cerramos el primero, dado que la función es recurrente, no es preciso recorrer todos los diálogos
				processObj.dialogs[0].onWindowClose();
			}else{
				//Si este proceso está en el array onProcessChangesEvents, lo borramos
				for (let i in this.onProcessChangesEvents){
					if (this.onProcessChangesEvents[i].id === processObj.id){
						this.onProcessChangesEvents.splice(i, 1);
					}
				}

				//No hay diálogos abiertos, destruímos la API del proceso
				processObj.api.destroy();
				delete processObj.api;
				//Eliminamos este proceso
				this.processes.splice(processIndex, 1);

				//Invocamos los eventos onProcessChange
				this.executeOnProcessChange();
			}
		}
	}

	/**
	 * Cierra un diálogo
	 * @param  {String} windowId  Identificador del diálogo
	 * @param  {String} processId Identificador del proceso al que pertenece el diálogo
	 */
	closeWindow(windowId, processId){
		//En primer lugar obtenemos el objeto del proceso
		let processObj = this.getProcessObj(processId);

		//Iteramos los diálogos que este proceso tiene abierto
		for (let i in processObj.dialogs){
			//Si este es el diálogo abierto
			if (processObj.dialogs[i].id === windowId){
				//Lo borramos
				processObj.dialogs.splice(i,1);

				//Si ya no quedan más diálogos cerramos el proceso...
				if (processObj.dialogs.length === 0 || processObj.status === this.PROCESS_CLOSING){
					this.kill(processId);
				}
				break;
			}
		}
	}


	addProcessChangesEvent(processObj){
		for (let i in this.onProcessChangesEvents){
			if (this.onProcessChangesEvents[i].id === processObj.id)
				return;
		}

		this.onProcessChangesEvents.push(processObj);
	}

	executeOnProcessChange(){
		//Ejecutamos los eventos al producirse cambios en procesos
		for (let i in this.onProcessChangesEvents){
			try{
				this.onProcessChangesEvents[i].module.onProcessChange();
			}catch(exception){
				console.error("Error: ",exception);
			}
			
		}
	}
}

export default ProcessManager;