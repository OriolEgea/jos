//Dependencias
import $ from "jquery";
//Estilos
import styles from "./styles/index.scss";
//Librerías requeridas
import Core from './libs/core/core.js';

class Index {
	constructor(){
		//Al finalizar la carga del documento, inicializamos
		$(document).ready(this.onDocumentLoad);
	}

	onDocumentLoad(){
		new Core();
	}
}

new Index();