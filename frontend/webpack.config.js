const path = require('path');

module.exports = {
	module: {
		rules: [
			{
		      test: /\.js$/,
		      exclude: /(node_modules|bower_components)/,
		      use: {
		        loader: 'babel-loader'
		      }
		    },
			{
	            test: /\.scss$/,
	            use: [{
	                loader: "style-loader" // creates style nodes from JS strings
	            }, {
	                loader: "css-loader" // translates CSS into CommonJS
	            }, {
	                loader: "sass-loader" // compiles Sass to CSS
	            }]
        	},
        	{
			  test: /\.(html)$/,
			  use: {
			    loader: 'html-loader',
			    options: {
			      attrs: [':data-src']
			    }
			  }
			}
		]
	},
	entry: {
		index: [
			'./src/index.js'
		]
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: path.join(__dirname, "dist"),
		compress: true,
		port: 9000
	}	
};