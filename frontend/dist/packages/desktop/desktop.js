class Desktop{
	constructor(){
		this.api = addProcess("Desktop", 1, this);
		this.$body = null;
		this.menuVisible = false;

		//Obtenemos jQuery
		this.api.getjQuery(this.onGetjQuery.bind(this));
	}

	onGetjQuery($){
		this.$ = $;

		//Obtenemos el html
		this.api.getFile("/desktop/dialogs/taskbar.html", this.onGetTaskbar.bind(this));
	}

	onGetTaskbar(layout){
		//Guardamos el html
		this.taskbar = layout;
		//Obtenemos la plantilla para cada app
		this.api.getFile("/desktop/dialogs/app.html", this.onGetApp.bind(this));
	}

	onGetApp(layout){
		this.app = layout;

		//Obtenemos la plantilla para cada proceso
		this.api.getFile("/desktop/dialogs/process.html", this.onGetProcess.bind(this));

	}

	onGetProcess(layout){
		this.process = layout;

		//Obtenemos el css
		this.api.getFile("/desktop/desktop.css", this.onGetCSS.bind(this));
	}

	onGetCSS(css){
		//Aplicamos los estilos custom
		this.api.loadCSS(css);

		//Obtenemos el fichero de traducción
		this.api.getFile("/desktop/i18n/es-ES.json", this.onGetLang.bind(this));
	}

	onGetLang(lang){
		//Aplicamos al HTML las etiquetas de traducción
		this.translatedLayout = this.api.layoutLang(this.taskbar, lang);

		//Nos mantenemos a la escucha del socket de listar aplicaciones
		this.api.listen("listApps", this.onListApps.bind(this));
		//Solicitamos al servidor un listado de las apps instaladas
		this.api.emit("listApps", "", ()=>{});

	}

	onListApps(apps){

		//Ordenamos por orden alfabético
		apps.sort(function (a, b){
			return (b.name < a.name);
		});

		//Abrimos diálogo
		this.$placeholder = this.api.createDialog("Desktop", {width: 500, height: 300});
		this.$body = this.$placeholder.parents("body");
		this.$body.append(this.translatedLayout);

		//Escondemos el diálogo
		this.$body.find(".window").remove();

		//Imprimimos las aplicaciones instaladas
		this.printApps(apps);

		//Definimos eventos sobre la barra de menú
		this.setEvents();

	}

	printApps(apps){
		//Iteramos las aplicaciones
		for (let i in apps){
			let app = apps[i];

			//Imprimimos esta aplicación
			let $app = this.$(this.app);
			$app.find(".app-name").html(app.name);
			$app.find(".app-icon").html(app.icon);
			$app.attr("data-package", app.packageName);
			this.$body.find(".apps-list").append($app);
		}

		//Nos mantenemos al tanto de cambios de eventos
		this.api.addProcessChangeEvent();
	}

	setEvents(){
		this.$body.find(".start-button").off("click");
		this.$body.find(".start-button").on("click", this.onMenuClick.bind(this));

		//Al pulsar sobre una aplicación
		this.$body.find(".app").off("click");
		this.$body.find(".app").on("click", this.runApp.bind(this));
	}

	onMenuClick(){
		if (this.menuVisible === true){
			this.hideMenu();
		}else{
			this.showMenu();
		}
	}

	showMenu(){
		this.$body.find(".apps-menu").show();
		this.menuVisible = true;
	}

	hideMenu(){
		this.$body.find(".apps-menu").hide();
		this.menuVisible = false;
	}

	runApp(event){
		//Obtenemos el nombre del paquete
		let packageName = this.$(event.currentTarget).attr("data-package");
		console.log("Starting "+packageName);
		this.api.run(packageName);
		this.hideMenu();

	}

	onProcessChange(){
		//Obtenemos los procesos
		let processes = this.api.getProcesses();
		//Seleccionamos el lugar donde los hemos de pintar
		let $placeholder = this.$body.find(".explorer-bar > .processes");
		//Vaciamos los procesos previos
		$placeholder.html(" ");
		//Iteramos los procesos
		for (let i in processes){
			let processObj = processes[i];
			//Si es el proceso Desktop, ignoramos
			if (processObj.name === "Desktop")
				continue;

			//Iteramos sus diálogos
			for (let j in processObj.dialogs){
				//Recopilaos datos
				let dialog = processObj.dialogs[j];
				//Modificamos parámetros del elemento de la taskbar
				let $process = this.$(this.process);
				$process.find(".process-tooltip").html(dialog.title);
				$process.attr("data-process", processObj.id);
				$process.attr("data-dialog", dialog.id);
				//Pintamos este diálogo...
				$placeholder.append($process);

				//Invocamos eventos
				this.setProcessEvents(dialog);
			}
		}
	}

	setProcessEvents(dialog){
		//Retiramos eventos previos
		let $process = this.$("body").find(".process[data-dialog='"+dialog.id+"']");
		$process.off("click");
		$process.on("click", this.onProcessClick.bind(this, dialog));
	}

	onProcessClick(dialog){
		dialog.onWindowUnminimize();
	}
}

new Desktop();