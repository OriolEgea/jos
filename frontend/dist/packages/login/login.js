class Login{
	constructor(){
		this.api = addProcess("Login", 1, this);
		this.setBodyBackground();

		this.$body = null;

		//Obtenemos el html
		this.api.getFile("/login/login.html", this.onGetLayout.bind(this));
	}

	setBodyBackground(){
		document.getElementsByTagName("body")[0].classList = ["login-screen"];
	}

	onGetLayout(layout){
		//Guardamos el html
		this.layoutHtml = layout;
		//Obtenemos el css
		this.api.getFile("/login/login.css", this.onGetCSS.bind(this));
	}

	onGetCSS(css){
		//Aplicamos los estilos custom
		this.api.loadCSS(css);

		//Obtenemos el fichero de traducción
		this.api.getFile("/login/i18n/es-ES.json", this.onGetLang.bind(this));
	}

	onGetLang(lang){
		//Aplicamos al HTML las etiquetas de traducción
		let translatedLayout = this.api.layoutLang(this.layoutHtml, lang);
		//Abrimos diálogo
		this.$placeholder = this.api.createDialog("Login", {width: 500, height: 300});
		this.$body = this.$placeholder.parents("body");
		this.$body.append(translatedLayout);

		//Escondemos el diálogo
		this.$body.find(".window").remove();

		this.setEvents();
	}

	setEvents(){
		this.$body.find(".login-button").off("click");
		this.$body.find(".login-button").on("click", this.login.bind(this));
	}

	login(){
		//Recuperamos usuario y contraseña introducidos
		let username = this.$body.find("[type='text']").val();
		let password = this.$body.find("[type='password']").val();

		let userError = (typeof username !== "string" || username.length < 1);
		let passwordError = (typeof password !== "string" || password.length < 6);

		if (userError){
			this.$body.find("[data-type='user-error']").show();
		}else{
			this.$body.find("[data-type='user-error']").hide();
		}

		if (passwordError){
			this.$body.find("[data-type='password-error']").show();
		}else{
			this.$body.find("[data-type='password-error']").hide();
		}

		if (!userError && !passwordError){
			let loginData = {
				username: username,
				password: password
			}
			this.api.emit('login',loginData, ()=>{});
		}
	}

	loginError(){
		this.$body.find("[data-type='user-error']").show();
		this.$body.find("[data-type='password-error']").show();
	}

	loginSuccess(){
		this.$body.find(".login-box").remove();
		this.api.process.kill();
	}
}

new Login();