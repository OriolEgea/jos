class Sysinfo{
	constructor(){
		this.api = addProcess("Sysinfo", 1, this);

		//Obtenemos el html
		this.api.getFile("/sysinfo/sysinfo.html", this.onGetTemplate.bind(this));
	}

	onGetTemplate(layout){
		this.layout = layout;
		//Obtenemos el css
		this.api.getFile("/sysinfo/sysinfo.css", this.onGetCSS.bind(this));
	}

	onGetCSS(css){
		//Aplicamos los estilos custom
		this.api.loadCSS(css);

		//Obtenemos el fichero de traducción
		this.api.getFile("/sysinfo/i18n/es-ES.json", this.onGetLang.bind(this));
	}

	onGetLang(lang){
		//Aplicamos al HTML las etiquetas de traducción
		this.translatedLayout = this.api.layoutLang(this.layout, lang);

		//Abrimos diálogo
		this.$placeholder = this.api.createDialog(lang.TITLE, {width: 500, height: 300});
		this.$placeholder.append(this.translatedLayout);

	}

	onListApps(apps){

		//Ordenamos por orden alfabético
		apps.sort(function (a, b){
			return (b.name < a.name);
		});

		
		this.$body = this.$placeholder.parents("body");
		this.$body.append(this.translatedLayout);

		//Escondemos el diálogo
		this.$body.find(".window").remove();

		//Imprimimos las aplicaciones instaladas
		this.printApps(apps);

		//Definimos eventos sobre la barra de menú
		this.setEvents();

	}

	printApps(apps){
		//Iteramos las aplicaciones
		for (let i in apps){
			let app = apps[i];

			//Imprimimos esta aplicación
			let $app = this.$(this.app);
			$app.find(".app-name").html(app.name);
			$app.find(".app-icon").html(app.icon);
			$app.attr("data-package", app.packageName);
			this.$body.find(".apps-list").append($app);
		}
	}

	setEvents(){
		this.$body.find(".start-button").off("click");
		this.$body.find(".start-button").on("click", this.onMenuClick.bind(this));

		//Al pulsar sobre una aplicación
		this.$body.find(".app").off("click");
		this.$body.find(".app").on("click", this.runApp.bind(this));
	}

	onMenuClick(){
		if (this.menuVisible === true){
			this.hideMenu();
		}else{
			this.showMenu();
		}
	}

	showMenu(){
		this.$body.find(".apps-menu").show();
		this.menuVisible = true;
	}

	hideMenu(){
		this.$body.find(".apps-menu").hide();
		this.menuVisible = false;
	}

	runApp(event){
		//Obtenemos el nombre del paquete
		let packageName = this.$(event.currentTarget).attr("data-package");
		console.log("Starting "+packageName);
		this.api.run(packageName);
		this.hideMenu();

	}
}

new Sysinfo();