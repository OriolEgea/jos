class ApiError{

	constructor(){
		this.sysApi = addProcess("ApplicationError", 1, this);

		//Obtenemos el layout
		this.sysApi.getFile("/api-error/api-error.html", this.onGetLayout.bind(this));
		
	}

	onGetLayout(layout){
		//Guardamos el layout
		this.layoutHtml = layout;

		//Obtenemos el idioma
		this.sysApi.getFile("/api-error/i18n/es-ES.json", this.onGetLang.bind(this));
		
	}

	onGetLang(lang){
		//Aplicamos al HTML las etiquetas de traducción
		let translatedLayout = this.sysApi.layoutLang(this.layoutHtml, lang);

		this.$placeholder = this.sysApi.createDialog("Error de sistema", {width: 500, height: 200});
		this.$placeholder.append(translatedLayout);
	}
}

new ApiError();