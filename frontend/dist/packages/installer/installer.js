/**
 * Instalador de jOS
 */
class Installer{

	constructor(){
		this.api = addProcess("Installer", 1, this);
		//Obtenemos el html
		this.api.getFile("/installer/installer.html", this.onGetLayout.bind(this));
	}

	onGetLayout(layout){
		//Guardamos el html
		this.layoutHtml = layout;
		//Obtenemos el css
		this.api.getFile("/installer/installer.css", this.onGetCSS.bind(this));
	}

	onGetCSS(css){
		//Aplicamos los estilos custom
		this.api.loadCSS(css);

		//Obtenemos el fichero de traducción
		this.api.getFile("/installer/i18n/es-ES.json", this.onGetLang.bind(this));
	}

	onGetLang(lang){
		//Aplicamos al HTML las etiquetas de traducción
		let translatedLayout = this.api.layoutLang(this.layoutHtml, lang);
		//Abrimos diálogo
		this.$placeholder = this.api.createDialog(lang.TITLE, {width: 500, height: 300});
		this.$placeholder.append(translatedLayout);

		//Al pulsar el botón de "Cancelar" matamos el proceso
		this.$placeholder.find("[data-type='cancel']").off("click");
		this.$placeholder.find("[data-type='cancel']").on("click", this.cancel.bind(this));

		//Al pulsar el botón de "Continuar" instalamos
		this.$placeholder.find("[data-type='start']").off("click");
		this.$placeholder.find("[data-type='start']").on("click", this.install.bind(this));
	}

	cancel(){
		this.api.process.kill();
	}

	install(){
		//Recuperamos usuario y contraseña introducidos
		let username = this.$placeholder.find("[type='text']").val();
		let password = this.$placeholder.find("[type='password']").val();

		let userError = (typeof username !== "string" || username.length < 1);
		let passwordError = (typeof password !== "string" || password.length < 6);

		if (userError){
			this.$placeholder.find("[data-type='user-error']").show();
		}else{
			this.$placeholder.find("[data-type='user-error']").hide();
		}

		if (passwordError){
			this.$placeholder.find("[data-type='password-error']").show();
		}else{
			this.$placeholder.find("[data-type='password-error']").hide();
		}

		if (!userError && !passwordError){
			let installData = {
				username: username,
				password: password
			}
			this.api.emit('install',installData, this.onInstall.bind(this));
		}
	}

	onInstall(){
		console.log("System has been installed");
	}
}

new Installer();